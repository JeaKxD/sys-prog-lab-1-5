﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.MemoryMappedFiles;
using Microsoft.Win32.SafeHandles;
using System.Threading;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[20];
            generateArray(array);
            outputArray(array);

            using (var mmFile = MemoryMappedFile.CreateFromFile(
                @"D:\lab.txt",
                System.IO.FileMode.Create, "Ran", 20 * 4))
            {
                var myAccessor = mmFile.CreateViewAccessor();
                var stream = mmFile.CreateViewStream();
                var handle = stream.SafeMemoryMappedViewHandle;
                myAccessor.WriteArray(0, array, 0, array.Length);
                unsafe
                {
                    byte* pointer = null;
                    handle.AcquirePointer(ref pointer);
                    Console.WriteLine("Press enter to sort");
                    Console.ReadLine();
                    Console.WriteLine("Start");

                    var size = 4 * 20;
                    for (int i = 0; i < size - 4; i += 4)
                    {
                        for (int j = 4; j < size - i; j += 4)
                        {
                            int first = *(pointer + j),
                                second = *(pointer + (j - 4));
                            if (first > second)
                            {
                                Swap(pointer + (j - 4));
                                Thread.Sleep(50);
                            }
                        }
                    }
                    Console.WriteLine("End");
                }
                Console.ReadLine();
            }
        }
        unsafe static void memorySort(byte* pointer)
        {
            var len = 4 * 20;
            for (var i = 4; i < len; i += 4)
            {
                for (var j = 0; j < len - i; j += 4)
                {
                    if (*pointer > *pointer + 4)
                    {
                        Swap(pointer);
                        *pointer = *(pointer + 4);
                    }
                }
            }
        }
        unsafe static void Swap(byte* pointer)
        {
            int temp;
            temp = *(pointer + 4);
            *(pointer + 4) = *pointer;
            *pointer = (byte)temp;
        }

        public static void generateArray(int[] array)
        {
            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(10, 100);
            }
        }

        public static void outputArray(int[] array)
        {
            Console.Write("Array: ");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]} ");
            }
            Console.WriteLine();
        }
    }
}
