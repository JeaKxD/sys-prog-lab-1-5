﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace Form1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static int[] toView = new int[20];
        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        public static void writeArray(int[] array)
        {
            using (var mmFile = MemoryMappedFile.OpenExisting("Ran"))
            {
                var myAccessor = mmFile.CreateViewAccessor();
                myAccessor.ReadArray(0, toView, 0, toView.Length);
            }
        }
        public static void generateArray(int[] array)
        {
            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(10, 100);
            }
        }

        public static void outputArray(int[] array, ListBox lb)
        {
            lb.Items.Clear();
            for (int i = 0; i < array.Length; i++)
            {
                lb.Items.Add(array[i]);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            writeArray(toView);
            outputArray(toView, listBox1);
        }
    }
}
